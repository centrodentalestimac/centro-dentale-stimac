Il centro dentale Štimac di Zagabria è composto da un team di dentisti esperti e con le migliori referenze, di modo che i pazienti usufruiscano in ogni momento del miglior servizio e che possano sentirsi in buone mani e sicuri del successo della terapia, indipendentemente dal ramo di odontoiatria in questione. 
Adress: Nova cesta 3, 10000 Zagabria, Croazia;
Website: https://www.stimac-dentista.it/	
Tel.: 0039 346 105 2107
Social media:
https://www.facebook.com/Stimac.centar
https://www.instagram.com/stimac.dentalcenter/
https://www.youtube.com/drstimac
https://www.linkedin.com/company/%C5%A1timac-centar-dentalne-medicine 
